﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class FrmGestionExtinguidores : Form
    {

        private DataSet dsExtinguidor;
        private BindingSource bsExtinguidor;
        public FrmGestionExtinguidores()
        {
            InitializeComponent();
            bsExtinguidor = new BindingSource();
            
        }

        public DataSet DsExtinguidores
        {
            set
            {
                dsExtinguidor = value;
            }

            get
            {
                return dsExtinguidor;
            }
        }
        private void FrmGestionExtinguidores_Load(object sender, EventArgs e)
        {
            bsExtinguidor.DataSource = DsExtinguidores;
            bsExtinguidor.DataMember = DsExtinguidores.Tables["Extinguidores"].TableName;
            dgvExtinguidores.DataSource = bsExtinguidor;
            dgvExtinguidores.AutoGenerateColumns = true;
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            FrmExtinguidores ext = new FrmExtinguidores();
            ext.DtExtinguidores = DsExtinguidores.Tables["Clientes"];
            ext.DsExtinguidores = dsExtinguidor;
            ext.ShowDialog();

        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvExtinguidores.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "Debe seleccionar una fila de la tabla para poder editar", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;

            DialogResult result = MessageBox.Show(this, "Realmente desea eliminar este registro?", "Mensaje del Sistema", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                DsExtinguidores.Tables["Clientes"].Rows.Remove(drow);
                MessageBox.Show(this, "Registro eliminado satisfactoriamente!", "Mensaje del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvExtinguidores.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "Debe seleccionar una fila de la tabla para poder editar", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DataGridViewRow gridRow = rowCollection[0];
            DataRow filas = ((DataRowView)gridRow.DataBoundItem).Row;

            FrmExtinguidores clt = new FrmExtinguidores();
            clt.DtExtinguidores = DsExtinguidores.Tables["Clientes"];
            clt.DsExtinguidores = DsExtinguidores;
            clt.DrExtinguidores = filas;
            clt.ShowDialog();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bsExtinguidor.Filter = string.Format("Marca like '*{0}*' or Tipo like '*{0}*' or Fecha_Recarga like '*{0}*' ", txtFinder.Text);

            }
            catch (InvalidExpressionException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
