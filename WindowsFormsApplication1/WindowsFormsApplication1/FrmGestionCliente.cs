﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class FrmGestionCliente : Form
    {
        private DataSet dsCliente;
        private BindingSource bsCliente;

        public FrmGestionCliente()
        {
            InitializeComponent();
            bsCliente = new BindingSource();
        }

        public DataSet DsCliente
        {
            set
            {
                dsCliente = value;
            }

            get
            {
                return  dsCliente;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FrmCliente clie = new FrmCliente();
            clie.DtCliente = DsCliente.Tables["Clientes"];
            clie.DsCliente = dsCliente;
            clie.ShowDialog();
        }

        private void FrmGestionCliente_Load(object sender, EventArgs e)
        {
            bsCliente.DataSource = DsCliente;
            bsCliente.DataMember = DsCliente.Tables["Clientes"].TableName;
            dgvCliente.DataSource = bsCliente;
            dgvCliente.AutoGenerateColumns = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvCliente.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "Debe seleccionar una fila de la tabla para poder editar", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;

            DialogResult result = MessageBox.Show(this, "Realmente desea eliminar este registro?", "Mensaje del Sistema", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                DsCliente.Tables["Clientes"].Rows.Remove(drow);
                MessageBox.Show(this, "Registro eliminado satisfactoriamente!", "Mensaje del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvCliente.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "Debe seleccionar una fila de la tabla para poder editar", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DataGridViewRow gridRow = rowCollection[0];
            DataRow filas = ((DataRowView)gridRow.DataBoundItem).Row;

            FrmCliente clt = new FrmCliente();
            clt.DtCliente= DsCliente.Tables["Clientes"];
            clt.DsCliente = DsCliente;
            clt.DrCliente = filas;
            clt.ShowDialog();

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bsCliente.Filter = string.Format("Cédula like '*{0}*' or Nombres like '*{0}*' or Apellidos like '*{0}*' ", txtFinder.Text);

            }
            catch (InvalidExpressionException ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
    }
}
