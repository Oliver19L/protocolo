﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class FrmExtinguidores : Form
    {
        private DataSet dsExtinguidores;
        private DataTable dtExtinguidores;
        private DataRow drExtinguidores;
        private BindingSource bsExtinguidores;
        public FrmExtinguidores()
        {
            InitializeComponent();
            bsExtinguidores = new BindingSource();
        }

        public DataSet DsExtinguidores
        {
            set
            {
                dsExtinguidores = value;
            }
            get
            {
                return dsExtinguidores;
            }
        }

        public DataRow DrExtinguidores
        {
            set
            {
                drExtinguidores = value;
                

                txtId.Text = drExtinguidores["Id"].ToString();
                txtCategoria.Text = drExtinguidores["Categoria"].ToString();
                txtTipo.Text = drExtinguidores["Tipo_Extinguidor"].ToString();
                txtMarca.Text = drExtinguidores["Marca"].ToString();
                txtCapacidad.Text = drExtinguidores["Capacidad"].ToString();
                txtUnidad.Text = drExtinguidores["Unidad_Medida"].ToString();
                txtLugar.Text = drExtinguidores["Lugar_Designado"].ToString();
                txtRecarga.Text = drExtinguidores["Fecha_Carga"].ToString();
            }
            get
            {
                return drExtinguidores;
            }
        }

        public DataTable DtExtinguidores
        {
            set
            {
                dtExtinguidores = value;
            }
            get
            {
                return dtExtinguidores;
            }
        }
        
           
        private void btnNuevo_Click(object sender, EventArgs e)
        {
            string Id, Category, Tipo, marca, capacidad, Unidad_Medida, Lugar_Designado, Fecha_Recarga;

            Id = txtId.Text;
            Category = txtCategoria.Text;
            Tipo = txtTipo.Text;
            marca = txtMarca.Text;
            capacidad = txtCapacidad.Text;
            Unidad_Medida = txtUnidad.Text;
            Lugar_Designado = txtLugar.Text;
            Fecha_Recarga = txtRecarga.Text;

            if (drExtinguidores != null)
            {
                DataRow drow = DtExtinguidores.NewRow();
                int Index = DtExtinguidores.Rows.IndexOf(drExtinguidores);

                drow["Id"] = drExtinguidores["Id"];
                drow["Categoria"] = Category;
                drow["Tipo_Extinguidor"] = Tipo;
                drow["Marca"] = marca;
                drow["Capacidad"] = capacidad;
                drow["Unidad_Medida"] = Unidad_Medida;
                drow["Lugar_Designado"] =Lugar_Designado;
                drow["Fecha_Carga"] = Fecha_Recarga;
                

                DtExtinguidores.Rows.RemoveAt(Index);
                DtExtinguidores.Rows.InsertAt(drow, Index);
                DtExtinguidores.Rows[Index].AcceptChanges();
                DtExtinguidores.Rows[Index].SetModified();

            }
            else
            {
                dsExtinguidores.Tables["Extinguidores"].Rows.Add(Id, Category, Tipo, marca, capacidad, Unidad_Medida, Lugar_Designado, Fecha_Recarga);
            }

            Dispose();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void FrmExtinguidores_Load(object sender, EventArgs e)
        {
            bsExtinguidores.DataSource = DsExtinguidores;
            bsExtinguidores.DataMember = DsExtinguidores.Tables["Extinguidores"].TableName;
        }
    }
}
