﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
        }

        private void clientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionCliente gest = new FrmGestionCliente();
            gest.DsCliente = dsCliente;
            gest.MdiParent = this;
            gest.Show();
        }

        private void empleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionExtinguidores ext = new FrmGestionExtinguidores();
            ext.DsExtinguidores = dsCliente;
            ext.MdiParent = this;
            ext.Show();
        }
    }
}
