﻿ using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class FormReporte : Form
    {

        private DataSet dsCliente;
        private DataRow drExtinguidores;
        public FormReporte()
        {
            InitializeComponent();
        }

        public DataSet DsCliente
        {

            set
            {
                dsCliente = value;
            }
            get
            {
                return dsCliente;
            }
        }
        public DataRow DrExtinguidores
        {
            set
            {
                drExtinguidores = value;
            }
        }

        private void FormReporte_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }
    }
}
