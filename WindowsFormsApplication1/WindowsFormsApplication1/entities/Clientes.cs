﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.entities
{
    class Clientes
    {

       private string Id, Cedula, nombre, apellido, celular, correo, dirrecion, municipio, departamento;

        public Clientes(string id, string cedula, string nombre, string apellido, string celular, string correo, string dirrecion, string municipio, string departamento)
        {
            Id = id;
            Cedula = cedula;
            this.nombre = nombre;
            this.apellido = apellido;
            this.celular = celular;
            this.correo = correo;
            this.dirrecion = dirrecion;
            this.municipio = municipio;
            this.departamento = departamento;
        }

        public string Apellido
        {
            get
            {
                return apellido;
            }

            set
            {
                apellido = value;
            }
        }

        public string Cedula1
        {
            get
            {
                return Cedula;
            }

            set
            {
                Cedula = value;
            }
        }

        public string Celular
        {
            get
            {
                return celular;
            }

            set
            {
                celular = value;
            }
        }

        public string Correo
        {
            get
            {
                return correo;
            }

            set
            {
                correo = value;
            }
        }

        public string Departamento
        {
            get
            {
                return departamento;
            }

            set
            {
                departamento = value;
            }
        }

        public string Dirrecion
        {
            get
            {
                return dirrecion;
            }

            set
            {
                dirrecion = value;
            }
        }

        public string Id1
        {
            get
            {
                return Id;
            }

            set
            {
                Id = value;
            }
        }

        public string Municipio
        {
            get
            {
                return municipio;
            }

            set
            {
                municipio = value;
            }
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }
    }
}
