﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.entities
{
    class Extinguidor
    {
        private string Id, Category, Tipo, marca, capacidad, Unidad_Medida, Lugar_Designado, Fecha_Recarga;

        public Extinguidor(string id, string category, string tipo, string marca, string capacidad, string unidad_Medida, string lugar_Designado, string fecha_Recarga)
        {
            Id = id;
            Category = category;
            Tipo = tipo;
            this.marca = marca;
            this.capacidad = capacidad;
            Unidad_Medida = unidad_Medida;
            Lugar_Designado = lugar_Designado;
            Fecha_Recarga = fecha_Recarga;
        }

        public string Capacidad
        {
            get
            {
                return capacidad;
            }

            set
            {
                capacidad = value;
            }
        }

        public string Category1
        {
            get
            {
                return Category;
            }

            set
            {
                Category = value;
            }
        }

        public string Fecha_Recarga1
        {
            get
            {
                return Fecha_Recarga;
            }

            set
            {
                Fecha_Recarga = value;
            }
        }

        public string Id1
        {
            get
            {
                return Id;
            }

            set
            {
                Id = value;
            }
        }

        public string Lugar_Designado1
        {
            get
            {
                return Lugar_Designado;
            }

            set
            {
                Lugar_Designado = value;
            }
        }

        public string Marca
        {
            get
            {
                return marca;
            }

            set
            {
                marca = value;
            }
        }

        public string Tipo1
        {
            get
            {
                return Tipo;
            }

            set
            {
                Tipo = value;
            }
        }

        public string Unidad_Medida1
        {
            get
            {
                return Unidad_Medida;
            }

            set
            {
                Unidad_Medida = value;
            }
        }
    }
}
