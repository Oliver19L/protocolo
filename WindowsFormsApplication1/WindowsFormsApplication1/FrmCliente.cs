﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class FrmCliente : Form
    {

        private DataSet dsCliente;
        private DataTable dtCliente;
        private DataRow drCliente;
        private BindingSource bsCliente;
        public FrmCliente()
        {
            InitializeComponent();
            bsCliente = new BindingSource();
        }

        public DataSet DsCliente
        {
            set
            {
                dsCliente = value;
            }
            get
            {
                return dsCliente;
            }

        }

        public DataRow DrCliente
        {
            set
            {
                drCliente = value;
                txtId.Text = drCliente["Id"].ToString();
                mskCedula.Text = drCliente["Cedula"].ToString();
                txtNombre.Text = drCliente["Nombre"].ToString();
                txtApellido.Text = drCliente["Apellido"].ToString();
                mskCelular.Text = drCliente["Celular"].ToString();

                txtCorreo.Text = drCliente["Correo"].ToString();
                txtDireccion.Text = drCliente["Direccion"].ToString();
                txtMunicipio.Text = drCliente["Municipio"].ToString();
                txtDepartamento.Text = drCliente["Departamento"].ToString();
            

        }
        get
            {
                return drCliente;
            }
        }

        public DataTable DtCliente
        {
            set
            {
                dtCliente = value;
            }
            get
            {
                return dtCliente;
            }
        }
        private void btnGuardar_Click(object sender, EventArgs e)
        {

            string Id,Cedula, nombre, apellido, celular, correo, dirrecion, municipio, departamento;

            Id = txtId.Text;
            Cedula = mskCedula.Text;
            nombre = txtNombre.Text;
            apellido = txtApellido.Text;
            celular = mskCelular.Text;
            correo = txtCorreo.Text;
            dirrecion = txtDireccion.Text;
            municipio = txtMunicipio.Text;
            departamento = txtDepartamento.Text;


            if (drCliente != null)
            {
                DataRow drow = DtCliente.NewRow();
                int Index = DtCliente.Rows.IndexOf(drCliente);

                drow["Id"] = drCliente["Id"];
                drow["Cedula"] = Cedula;
                drow["Nombre"] = Cedula;
                drow["Apellido"] = Cedula;
                drow["Celular"] = Cedula;
                drow["Correo"] = Cedula;
                drow["Direccion"] = Cedula;
                drow["Municipio"] = Cedula;
                drow["Departamento"] = Cedula;

                DtCliente.Rows.RemoveAt(Index);
                DtCliente.Rows.InsertAt(drow, Index);
                DtCliente.Rows[Index].AcceptChanges();
                DtCliente.Rows[Index].SetModified();

            }
            else
            {
                dsCliente.Tables["Clientes"].Rows.Add(Id,Cedula, nombre, apellido, celular, correo, dirrecion, municipio, departamento);
            }


            Dispose();

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void FrmCliente_Load(object sender, EventArgs e)
        {
            bsCliente.DataSource = DsCliente;
            bsCliente.DataMember = DsCliente.Tables["Clientes"].TableName;
        }
    }
}
